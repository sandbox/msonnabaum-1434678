<?php


/**
 * Implementation of hook_drush_command().
 */
function apc_drush_command() {
  $items['apc-stats'] = array(
    'description' => '',
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
    'options' => array(
      'url' => 'The web accessible url of where the command is being run.',
    ),
  );

  return $items;
}


function drush_apc_stats() {
  if (!$url= drush_get_option('url')) {
    $url = 'http://localhost';
  }

  $file_name = 'check_apc_stats.php';
  $file_contents = '
<?php
$apc_opcode = apc_cache_info("opcode", 1);
$apc_info = apc_sma_info();
$stats = array(
  "Used" => round($apc_opcode["mem_size"] / pow(1024, 2), 2),
  "Free" => round(($apc_info["avail_mem"] - $apc_opcode["mem_size"])/ pow(1024, 2), 2),
);
print json_encode($stats);';

  file_put_contents($file_name, $file_contents);
  $json = file_get_contents("$url/check_apc_stats.php");
  unlink($file_name);
  drush_print($json);
}
